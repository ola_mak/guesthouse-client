import java.io.IOException;
import java.util.Scanner;

public class GuesthouseClientApplication {

    public static void main(String[] args) throws IOException {
        Scanner sc= new Scanner(System.in);

        System.out.println("Put check-in date. Please use dd-MMM-yyyy format of date.");
        String checkIn= sc.nextLine();
        System.out.println("Put check-out date. Please use dd-MMM-yyyy format of date.");
        String checkOut= sc.nextLine();
        String freeRooms = GuesthouseClient.getFreeRooms(checkIn, checkOut);
        System.out.println(freeRooms);


        System.out.println("To book a room, please type the ID");
        String roomId = sc.nextLine();
        System.out.println("Put your email");
        String email = sc.nextLine();
        String token = GuesthouseClient.bookRoom(Long.valueOf(roomId), email, checkIn, checkOut);
        System.out.println("Reservation is saved successfully. If you want cancel: type \"1\", if not: type \"0\"");
        int cancellation = Integer.parseInt(sc.nextLine());
        if (cancellation == 1) {
            GuesthouseClient.cancelBooking(token);
        } else {
            System.out.println("See you soon!");
        }

    }

}
