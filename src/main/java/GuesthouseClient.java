import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class GuesthouseClient {

    public static String getFreeRooms(String fromDate, String toDate) throws IOException {

        Map<String, String> mapOfParameters = new HashMap<String, String>();
        mapOfParameters.put("status", "free");
        mapOfParameters.put("from-date", fromDate);
        mapOfParameters.put("to-date", toDate);

        String parameters = buildStringParameters(mapOfParameters);

        URL url = new URL(createURLWithPort("/api/rooms?") + parameters);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod("GET");
        httpConnection.setDoOutput(true);

        String response = readResponse(httpConnection);

        httpConnection.disconnect();

        return response;
    }

    public static String bookRoom(Long roomID, String emailAddress, String fromDate, String toDate) throws IOException {

        String body = createBody(emailAddress, fromDate, toDate);
        URL url = new URL(createURLWithPort("/api/rooms/" + roomID + "/bookings"));
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod("POST");
        httpConnection.setDoOutput(true);
        httpConnection.setRequestProperty("Content-Type", "application/json");

        OutputStream os = httpConnection.getOutputStream();
        os.write(body.getBytes("UTF-8"));
        os.close();

        httpConnection.connect();
        String response = readResponse(httpConnection);
        httpConnection.disconnect();

        return response;
    }

    public static void cancelBooking(String token) throws IOException {
        String encodedToken = URLEncoder.encode(token, StandardCharsets.UTF_8.toString());
        URL url = new URL(createURLWithPort("/api/rooms/bookings?token=" + encodedToken));

        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setDoOutput(true);
        httpConnection.setRequestProperty(
                "Content-Type", "application/x-www-form-urlencoded");
        httpConnection.setRequestMethod("DELETE");
        httpConnection.connect();
        readResponse(httpConnection);

    }

    private static String buildStringParameters(Map<String, String> params) {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result.toString();

    }

    private static String createURLWithPort(String uri) {
        return "http://localhost:8080" + uri;
    }

    private static String readResponse(HttpURLConnection httpConnection) throws IOException {

        StringBuilder response = new StringBuilder();

        BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        String line = null;
        while ((line = in.readLine()) != null) {
            response.append(line);
        }
        in.close();

        return response.toString();
    }

    private static String createBody(String emailAddress, String fromDate, String toDate) {

        return "{\"emailAddress\":\"" + emailAddress + "\", \"fromDate\":\"" + fromDate + "\", \"toDate\":\"" + toDate + "\"}";
    }

}
